let menu = new Phaser.Scene('menu');
menu.preload=function preload ()
{
    this.load.setBaseURL('assets');
    this.load.image('kotak kuning','start.png');
    this.load.bitmapFont('carrier_command', 'fonts/bitmap/topaz.png', 'fonts/bitmap/topaz.xml');
    this.load.atlas('monster1','mon1/mon1anim.png','mon1/mon1anim.json');
    this.load.atlas('monster2','mon2/mon2anims.png','mon2/mon2anims.json');
    this.load.atlas('monster3','mon3/mon3anims.png','mon3/mon3anims.json');
}
var strTower;
menu.create=function create ()
{   
    if(strTower==null)strTower="buyucu1.png";
    this.cameras.main.setBackgroundColor('#000000');
    var present = this.add.bitmapText(250, 80, 'carrier_command',"momon's",34);
    var title = this.add.bitmapText(250, 150, 'carrier_command','Castle',100);

    var btnSetting=this.add.bitmapText(config.width/2-80,config.height-220, 'carrier_command','Setting',50);
    btnSetting.setInteractive();
    btnSetting.on('pointerup',function(){
        this.scene.scene.start('setting');
    });
    var creditBtn=this.add.bitmapText(config.width/2-200,config.height-300, 'carrier_command','Credits To Febrian N',50);
    
    var btn = this.add.image(config.width/2,config.height,'kotak kuning');
    btn.scaleX=0;
    btn.scaleY=0;
    btn.setInteractive();
    btn.on('pointerup',function(){
        // this.scene.scene.start('main');
        this.scene.scene.start('main', { tower: strTower });
    });
    var momon1=this.physics.add.sprite(430,config.height/2-180,'monster3','die_00_delay-0.08s.gif.png');
    momon1.setScale(2);
    var momon2=this.physics.add.sprite(190,config.height/2-120,'monster1','attack_3_delay-0.04s.gif');
    momon2.setScale(4);


    this.tweens.add({
        targets:[btn],
        y:config.height/1.2,
        duration:500,
        scaleX:0.5,
        scaleY:0.5,
        ease: 'Bounce.easeOut'
    });
}

menu.init=function init (data){
    strTower=data.tower;
}

