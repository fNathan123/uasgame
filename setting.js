let setting = new Phaser.Scene('setting');
setting.preload=function preload ()
{
    
    this.load.setBaseURL('assets');
    this.load.image('pagain','playagain.jpg');
    this.load.image('previewTower','buyucu2.png');
    this.load.image('previewTower2','buyucu1.png');
    this.load.bitmapFont('carrier_command', 'fonts/bitmap/topaz.png', 'fonts/bitmap/topaz.xml');
}

var strTower;
setting.create=function create ()
{
    console.log('msk setting');
    this.cameras.main.setBackgroundColor('#000000');
    var strTitle = this.add.bitmapText(100,100, 'carrier_command','Choose Tower :',50);
    var tower1=this.add.image(config.width/2-200,config.height/2,'previewTower');
    var tower2=this.add.image(config.width/2+200,config.height/2,'previewTower2');
    tower1.setInteractive();
    tower2.setInteractive();
    tower1.setScale(0.4);
    tower2.setScale(0.4);
    //default
    tower1.on('pointerup',function(){
        strTower="buyucu2.png";
    });
    tower2.on('pointerup',function(){
        strTower="buyucu1.png";
    });


    var savebtn = this.add.bitmapText(config.width/2-80,config.height-120, 'carrier_command','Save',50);
    savebtn.setInteractive();
    savebtn.on('pointerup',function(){
        // this.scene.scene.start('menu');
        this.scene.scene.start('menu', { tower: strTower });
    });
}

