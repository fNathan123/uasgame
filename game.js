
    var config = {
        type: Phaser.AUTO,
        width: 800,
        height: 600,
        backgroundColor:'#71c5cf',
        physics: {
            default: 'arcade',
            arcade: {
                debug:false,
                gravity: { y: 0 }
            }
        },
        scene: [menu,main,restartMenu,setting]
    };

    var game = new Phaser.Game(config);


