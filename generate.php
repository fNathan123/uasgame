<?php
    echo '{"frames": {';
    $multiplierx= 0;
    $multipliery=0;
    $x=512;
    $y=512;
    $index=0;
    for($i=0;$i<8;$i++){
        for($j=0;$j<8;$j++){
            $xtemp=$x*$multiplierx;
            $ytemp=$y*$multipliery;
            echo '"explode'.$index.'.png": {';
            echo '			
                "frame": {
				"x":'.$xtemp.',
				"y": '.$ytemp.',
				"w": 512,
				"h": 512
            },';
            echo'
            "rotated": false,
			"trimmed": false,
			"spriteSourceSize": {
				"x": '.$xtemp.',
				"y": '.$ytemp.',
				"w": 512,
				"h": 512
            },';
            echo '
            "sourceSize": {
				"w": 512,
				"h": 512
			}
            ';
            echo '},';
            $multipliery++;
            $index++;
        }
        $multipliery=0;
        $multiplierx++;
    }
    echo '}}';
?>