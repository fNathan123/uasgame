let main = new Phaser.Scene('main');
class Bomb{
    constructor(spawnX, spawnY, spriteBomb,initVeloY) {
        this.spawnX = spawnX;
        this.spawnY = spawnY;
        this.spriteBomb= spriteBomb;
        this.initVeloY=initVeloY;
    }
    updateVelo(){
        var newVelox=this.spriteBomb.body.velocity.x*0.5;
        var newVeloy=-Math.sqrt(this.initVeloY+2*1000*this.spawnY)*0.5;
        this.spawnY=this.spawnY*0.5;
        this.initVeloY=Math.abs(newVeloy);
        //var newVeloy=-Math.sqrt(2*1000*this.spawnY);
        //console.log(newVelox+" "+newVeloy);
        this.spriteBomb.setVelocity(newVelox,newVeloy);
    }
}

class Monster{
    constructor(spriteMonster){
        this.spriteMonster=spriteMonster;
        this.alive=true;
    }
    setAlive(isAlive){
        this.alive=isAlive;
    }
}
var strTower;
main.init=function init (data){
    strTower=data.tower;
}

main.preload=function(){
    this.load.setBaseURL('assets');
    this.load.image('bomb','bomb.png');
    this.load.image('tower',strTower);
    this.load.atlas('terrain','tile2.png','terrain.json');
    this.load.atlas('monster1','mon1/mon1anim.png','mon1/mon1anim.json');
    this.load.atlas('monster2','mon2/mon2anims.png','mon2/mon2anims.json');
    this.load.atlas('monster3','mon3/mon3anims.png','mon3/mon3anims.json');
    this.load.atlas('explosion','explosion/explosion4.png','explosion/explosion4.json');
    this.load.bitmapFont('carrier_command', 'fonts/bitmap/topaz.png', 'fonts/bitmap/topaz.xml');

    // this.load.atlas('boss','boss/bosswalk.png','boss/bosswalk.json');
}

var platforms;
var towers;
var seconCoor;
var power;
var timeArr = [];
var monsterArr=[];
var monsterIsHit=[];
var spawnTimer;
var towerHP=10;
var maxTowerHP;
var towerHPString;



main.create=function(){
    // this.scene.start();
    platforms = this.physics.add.staticGroup();
    towers = this.physics.add.staticGroup();
    this.bombs =this.add.group();
    this.monsters=this.add.group();
    for(i=0;i<10;i++){
        this.generatePlatform((110*i),config.height);
    }
    this.generateTower(config.width-50,config.height/2-30);
    maxTowerHP=towerHP;
    towerHPString = this.add.bitmapText(config.width-100,config.height/2-30, 'carrier_command',towerHP+"/"+maxTowerHP,34);
    //this.generateBomb(config.width/2,0);

    this.generateAnims();
    // this.generateMonster(0,config.height/2,1,2000);
    // this.generateMonster(0,config.height/2,2,2000);
    // this.generateMonster(0,config.height/2,3,2000);
    //  var s='monster3';
    // var ex=this.physics.add.sprite(config.width/2,config.height/2,'explosion','explode1.png');
    // ex.play('explode');
    // var ex2=this.physics.add.sprite(config.width/2,config.height/2,'monster3','die_00_delay-0.08s.gif.png');
    // ex2.play('die3');

    this.physics.add.collider(this.bombs,platforms,()=>{

    });

    this.input.on('pointerdown', ()=> {
        x=this.input.mousePointer.x;
        y=this.input.mousePointer.y;
        seconCoor=[x,y];
        console.log(x+" "+y);
    });

    this.input.on('pointerup', ()=> {
        console.log('up');
        this.generateBomb(config.width,config.height/2);
    });
    // spawnTimer = this.time.addEvent({ delay: 500, callback:()=>{
    //     var spawn=Math.random();
    //     console.log('spawn');
    //     if(spawn>0.6){
    //         var monsterType= Math.floor(Math.random() * Math.floor(4));
    //         if(monsterType==4)monsterType--;
    //         if(monsterType==0)monsterType++;
    //         this.generateMonster(0,config.height/2,monsterType,2000);
    //     }
    //     }, callbackScope: this, loop: true });
    }

var waveCounter=1;
var monster=5;
var level=1;
var lastMonster=5;
var monsterAlive;
var startWave=false;
var bmpText;
var timer;
var time=3;

var mult=0;
main.update=function(){
    if (this.input.activePointer.isDown){
        this.debug();
    }
    if(!startWave){  
        var str='Wave :'+waveCounter;
        if(bmpText==null)bmpText = this.add.bitmapText(10, 100, 'carrier_command',str,34);
        if(timer==null){
            timer = this.time.addEvent({ delay: 1000, callback:()=>{
                time--;
                console.log('msk waktu');
            }, callbackScope: this, loop: true });
        }
        if(time==0){
            timer.destroy();
            timer=null;
            monsterArr=[];
            monsterIsHit=[];
            monster=Math.ceil(lastMonster+level*mult);
            lastMonster=monster;
            monsterAlive=monster;
            mult+=0.2;
            level++;
            startWave=true;

        }


    }
    if(startWave){
        if(bmpText!=null && bmpText.visible){
            bmpText.visible=false;
            console.log('start wave');
            console.log('monster : '+monsterAlive);
            bmpText=null;
        }
        
        if(monsterAlive==0){
            waveCounter++;
            console.log('transisi');
            spawnTimer.destroy();
            spawnTimer=null;
            time=3;
            startWave=false;        
        }
        if(spawnTimer==null && monster>0){
            spawnTimer = this.time.addEvent({ delay: 800, callback:()=>{
            var spawn=Math.random();
            if(spawn>0.3){
                var monsterType= Math.floor(Math.random() * Math.floor(4));
                if(monsterType==4)monsterType--;
                if(monsterType==0)monsterType++;
                if(monster>0)this.generateMonster(0,config.height/2,monsterType,2000);
                monster--;
            }
        }, callbackScope: this, loop: true });
        }
    }
    if(towerHP<=0){
        this.scene.pause();
        this.restart();
    }

    
}
main.restart=function(){
    waveCounter=1;
    monster=5;
    level=1;
    lastMonster=5;
    startWave=false;
    timer=null;
    time=3;
    towerHP=20;
    spawnTimer.destroy();
    spawnTimer=null;
    mult=0;
    this.scene.start('restartMenu');
}
main.debug=function(x,y){
    console.log('msk');
}

main.generatePlatform=function(x,y){
    var terrain=platforms.create(x,y,'terrain');
    terrain.setScale(2);
    terrain.body.y=y-100;
    terrain.body.width = 100;
    terrain.body.height = 100;
    terrain.setCollideWorldBounds(true);
}
main.generateTower=function(x,y){
    // var tower=this.physics.add.sprite(x,y,'tower');
    var tower=towers.create(x,y,'tower');
    tower.setGravityY(0);
    tower.setScale(0.8);
    // this.towers.add(tower);
    
}
main.generateBomb=function(x,y){
    power=1;
    var velox=(seconCoor[0]-x)*power;
    var veloy=(seconCoor[1]-y)*power;
    var bomb=this.physics.add.sprite(x,y,'bomb');
    console.log(velox,veloy);
    bomb.setVelocity(velox,veloy);
    bomb.setScale(0.4);
    bomb.setGravityY(1000);
    //this.bombs.add(bomb);
    var b=new Bomb(x,y,bomb,veloy);
    this.physics.add.collider(bomb,platforms,()=>{
        b.updateVelo();
    });
    this.physics.add.collider(bomb,this.monsters,(gameObject,gameObject2)=>{
        gameObject.destroy();
        //this.time.destroy();
        var ex=this.physics.add.sprite(gameObject.x,gameObject.y,'explosion','explode1.png');
        ex.setSize(0.2*ex.width,0.2*ex.height,true);
        var temp=gameObject2.name+"";
        // console.log('debug '+temp.charAt(1));
        var monIdx=parseInt(temp.charAt(1));

        this.physics.add.collider(ex,this.monsters,(gameObject,gameObject3)=>{

            if(monsterIsHit[monIdx]==false){
                this.monsterHit(gameObject3);
                monsterIsHit[monIdx]=true;
            }
            
        });

        ex.on('animationcomplete',()=>{
            this.finishExplode(monIdx,ex);
        }, this);
        ex.play('explode');
        // var monIdx=parseInt(monName.charAt(1));
        // monsterArr[monIdx]--;
        // if(monsterArr[monIdx]<=0){
        //     gameObject2.play(mon);
        //     gameObject2.on('animationcomplete', ()=>{
        //         gameObject2.destroy();
        //         monsterAlive--;
        //         gameObject2.setActive(false);
        //         if(monName.length>1){
        //             var idx=parseInt(monName.charAt(2));
        //             timeArr[idx].destroy();
        //             //timeArr.splice[idx];
        //         }
        //     }, this);
        // }
        // else{
        //     // gameObject2.setVelocityY(gameObject2.velocityY+20);
        //     gameObject2.removeInteractive();
        //     gameObject2.setVelocityY(gameObject2.velocityY+20);
        // }
        this.monsterHit(gameObject2);

        
    });
}
main.finishExplode=function(monIdx,ex){
    this.tweens.add({
        targets: ex,
        duration: 500,
        alpha: 0
    });
    ex.destroy();
    monsterIsHit[monIdx]=true;
}
main.monsterHit=function(monster){
    var temp=monster.name+"";
    // console.log('debug '+temp.charAt(1));
    console.log('msk monster hit');
    var monIdx=parseInt(temp.charAt(1));
    console.log('monster'+temp+' LP:'+monsterArr[monIdx]);
    monsterArr[monIdx]--;
    if(monsterArr[monIdx]<=0 && monster.active){
    monster.play('die'+temp.charAt(0));
    monster.on('animationcomplete', ()=>{
            monster.destroy();
            monsterAlive--;
            console.log('monster : '+monsterAlive);
            monster.setActive(false);
            if(monName.length>1){
                var idx=parseInt(temp.charAt(2));
                timeArr[idx].destroy();
                //timeArr.splice[idx];
            }
        }, this);
    }
    else{
        // gameObject3.setVelocityY(gameObject3.velocityY+20);
        // console.log('monster'+temp+' LP:'+monsterArr[monIdx]);
        // console.log(monster.x+' '+monster.y);
        //console.log('velo:'+monster.body.velocity.x);
        monster.setVelocityX(70);
        // monster.body.setAcceleration(30, 0);
        // console.log(monster.x+' '+monster.y);
    }

}
main.generateMonster=function(x,y,monster,delay){
    var velox=70;
    var monString='monster'+monster;
    var animStringWalk='running'+monster;
    var animStringAttack='attack'+monster;
    var animStringIddle='iddle'+monster;
    var animStringDie='die'+monster;
    // console.log(monString);
    var mon=this.physics.add.sprite(x,y,'monster2','walk_0_delay-0.08s.gif');
    mon.play(animStringWalk);
    var monClass=new Monster(mon);
    var bodyHeight=30;
    if(monster==2)bodyHeight=50;
    mon.body.setSize(30 , bodyHeight , true);
    mon.setScale(3);
    this.monsters.add(mon);
    var baseHealth=1;
    var additionalHealth=Math.floor(level/2);
    monsterArr.push(baseHealth+additionalHealth);
    monsterIsHit.push(false);
    mon.setName(monster+""+monsterArr.length-1);
    // this.physics.add.collider(mon,this.bombs,()=>{
    //     mon.play(animStringDie);
    //     mon.destroy();
    // });
    this.physics.add.collider(mon,platforms,()=>{
        
    });
    this.physics.add.collider(this.monsters,towers,(gameObject,gameObject2)=>{
        var temp=gameObject.name+"";
        if(temp.length>2)monIdx=parseInt(temp.charAt(2));
        if(temp.length<3){
            var stringAttack='attack'+parseInt(temp.charAt(0));
            var stringIddle='iddle'+parseInt(temp.charAt(0));
            gameObject.play(stringAttack);
            gameObject.on('animationcomplete', ()=>{
                gameObject.play(stringIddle);
            }, this);
            timeArr.push(this.time);
            timeArr[timeArr.length-1].addEvent({ delay: delay, callback:()=>{
                if(gameObject.active) {
                    gameObject.play(stringAttack);
                    towerHP--;
                    towerHPString.destroy();
                    towerHPString = this.add.bitmapText(config.width-100,config.height/2-30, 'carrier_command',towerHP+"/"+maxTowerHP,34);
                }
                
                // mon.on('animationcomplete', ()=>{
                //     mon.play(animStringIddle);
                // }, this);
            }, callbackScope: this, loop: true });
            gameObject.setName(gameObject.name+""+timeArr.length-1);
        }
    });
    mon.setVelocity(velox,0);
    if(monster!=3){
        mon.setGravityY(1000);
        mon.body.setMaxVelocity(velox,1000);
    }
    else {
        mon.setGravityY(0);
        mon.body.setMaxVelocity(velox,0);
    }


}

main.generateAnims=function(){
    this.anims.create(
        {key:'running1',
        frames:this.anims.generateFrameNames('monster1',{start:0,end:4,zeroPad:1,prefix:'walk_',suffix:'_delay-0.08s.gif'}),
        repeat:-1,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'running2',
        frames:this.anims.generateFrameNames('monster2',{start:0,end:3,zeroPad:1,prefix:'walk_',suffix:'_delay-0.08s.gif'}),
        repeat:-1,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'running3',
        frames:this.anims.generateFrameNames('monster3',{start:0,end:4,zeroPad:1,prefix:'walk_',suffix:'_delay-0.1s.gif'}),
        repeat:-1,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'attack1',
        frames:this.anims.generateFrameNames('monster1',{start:0,end:7,zeroPad:1,prefix:'attack_',suffix:'_delay-0.08s.gif'}),
        repeat:0,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'attack2',
        frames:this.anims.generateFrameNames('monster2',{start:0,end:3,zeroPad:1,prefix:'attack_',suffix:'_delay-0.08s.gif'}),
        repeat:0,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'attack3',
        frames:this.anims.generateFrameNames('monster3',{start:0,end:4,zeroPad:1,prefix:'attack_',suffix:'_delay-0.1s.gif'}),
        repeat:0,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'iddle1',
        frames:this.anims.generateFrameNames('monster1',{start:0,end:0,zeroPad:1,prefix:'attack_',suffix:'_delay-0.08s.gif'}),
        repeat:0,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'iddle2',
        frames:this.anims.generateFrameNames('monster2',{start:0,end:3,zeroPad:1,prefix:'frame_',suffix:'_delay-0.1s.gif'}),
        repeat:0,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'iddle3',
        frames:this.anims.generateFrameNames('monster3',{start:0,end:4,zeroPad:1,prefix:'walk_',suffix:'_delay-0.1s.gif'}),
        repeat:1,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'die1',
        frames:this.anims.generateFrameNames('monster1',{start:0,end:7,zeroPad:2,prefix:'die_',suffix:'_delay-0.08s.gif'}),
        repeat:0,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'die2',
        frames:this.anims.generateFrameNames('monster2',{start:0,end:8,zeroPad:2,prefix:'die_',suffix:'_delay-0.08s.gif'}),
        repeat:0,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'die3',
        frames:this.anims.generateFrameNames('monster3',{start:0,end:9,zeroPad:2,prefix:'die_',suffix:'_delay-0.08s.gif'}),
        repeat:0,
        frameRate:10
        }
    );
    this.anims.create(
        {key:'explode',
        frames:this.anims.generateFrameNames('explosion',{start:0,end:7,zeroPad:1,prefix:'explode',suffix:'.png'}),
        repeat:0,
        frameRate:10
        }
    );
    //create die anims


}